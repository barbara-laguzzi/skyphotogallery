$(function(items) {
    var Mustache = require('mustache');

    var feed;
    $.getJSON('https://www.reddit.com/r/OldSchoolCool/top.json', function(result) {
        feed = result.data;
        items = [feed.children];
        var template = $('#template').html();
        Mustache.parse(template);   // optional, speeds up future uses
        var rendered = Mustache.render(template, feed);
        $('#target').html(rendered);
    }); //getJSON



}); //function